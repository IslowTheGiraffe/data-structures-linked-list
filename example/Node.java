package example;

public class Node
{
  public int value;
  public boolean petOwner;
  public String name;
  public Node next;
  
  public Node(String n, int valueInput, boolean petOwner)
  {
    name = n;
    value = valueInput;
    this.petOwner = petOwner;
    next = null;
  }
}