package example;

public class Main {


    public static void main(String[] args) {
        System.out.println("Hello Yetis");
        Node headNode = new Node("Eddie",23,true);
//        System.out.println("Name : " + headNode.name);
//        System.out.println("Age : " + headNode.value);
//        System.out.println("Is pet owner? : " + headNode.petOwner);

        Node davidNode = new Node("David",25,false);
//        System.out.println("Name : " + davidNode.name);
//        System.out.println("Age : " + davidNode.value);
//        System.out.println("Is pet owner? : " + davidNode.petOwner);

        headNode.next = davidNode;
        System.out.println("Name : " + headNode.name);
        System.out.println("Age : " + headNode.value);
        System.out.println("Is pet owner? : " + headNode.petOwner);
        System.out.println("Who's behind me? : " + headNode.next.name);

        Node kiwiNode = new Node("Bubby", 1, false);

        AddLastNode(headNode,kiwiNode);
        headNode = AddFirstNode(headNode,new Node("DrEggplant",10,false));
        AddLastNode(headNode,new Node("Angela",23,true));

        printAll(headNode);

        int numberOfNodes = HowManyNodes(headNode);
        System.out.println(numberOfNodes);

        boolean isEven = false;
        if(numberOfNodes % 2 == 0)
            isEven = true;
        else
            isEven = false;

        System.out.println("Is it even?: " + isEven);


    }

    public static void printAll(Node HeadNode)
    {
        while(HeadNode != null)
        {
            System.out.println("Name : " + HeadNode.name);
            HeadNode = HeadNode.next;
        }
    }

    public static Node AddFirstNode(Node HeadNode, Node newHeadToBe)
    {
        newHeadToBe.next = HeadNode;
        return newHeadToBe;
    }
    public static void AddLastNode(Node HeadNode, Node newNowBeingAdded)
    {
        while(HeadNode.next != null)
        {
            HeadNode = HeadNode.next;
        }

        HeadNode.next = newNowBeingAdded;
    }

    public static int HowManyNodes(Node HeadNode)
    {
        int numberOfNodes = 0;
        while(HeadNode != null)
        {
            HeadNode = HeadNode.next;
            numberOfNodes++;
        }

        return numberOfNodes;
    }
}
